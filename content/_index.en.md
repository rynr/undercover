---
sitemap:
  changefreq: weekly
  priority: 1
---
UnderCover is keeping LIVE music alive!. A classic Rock 4 piece band from the UK and Germany, playing the best music all the way back from the 70’s to today, making that unholy sound that everyone loves. Well established in the Munich area and with a loyal following as a result of their reputation for top notch shows they’ll have you singing along from the first to the last beat.  
From the hottest bars to the biggest festival UnderCover always brings it to the stage and at the end of the show you’ll always be demanding more.

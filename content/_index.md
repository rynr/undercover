---
sitemap:
  changefreq: weekly
  priority: 1
---
UnderCover hält die LIVE-Musik am Leben! Eine klassische 4-köpfige Rockband aus Großbritannien und Deutschland, die die beste Musik von den 70er Jahren bis heute spielt und diesen unheiligen Sound macht, den jeder liebt. Die Band hat sich in der Region München etabliert und hat eine treue Fangemeinde, die sie mit ihren erstklassigen Shows vom ersten bis zum letzten Takt zum Mitsingen bringt.  
Ob in den angesagtesten Bars oder auf den größten Festivals, UnderCover bringen es immer auf die Bühne und am Ende der Show werden Sie immer nach mehr verlangen.
